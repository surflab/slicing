
/*

This file is part of slicing. 




slicing is free software: you can redistribute it and/or modify

it under the terms of the GNU General Public License as published by

the Free Software Foundation, either version 3 of the License, or

(at your option) any later version.




slicing is distributed in the hope that it will be useful,

but WITHOUT ANY WARRANTY; without even the implied warranty of

MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

GNU General Public License for more details.




You should have received a copy of the GNU General Public License

along with this program. If not, see <http://www.gnu.org/licenses/>.

*/


// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <array>
#include <stack>   
#include <sstream>
#include <iostream>
#include <math.h>
#include <ctime>
// Include GLEW
#include <GL/glew.h>
// Include GLFW
#include <glfw3.h>
// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
using namespace glm;
using namespace std;
#include <AntTweakBar.h>

#include <common/shader.hpp>
#include <common/controls.hpp>
#include <common/objloader.hpp>
#include <common/vboindexer.hpp>

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>

#include "rendering.hpp"

typedef OpenMesh::PolyMesh_ArrayKernelT<> Mesh;

int main(int argc, char** argv)
{
    if(argc>2 || argc<1)
    {
        cout << "Correct usage: " << argv[0] << " $FILENAME\n";
        return 0;
    }
    clock_t begin=clock();

    Mesh myMesh;
    if(argc == 2){
        OpenMesh::IO::read_mesh(myMesh, argv[1]);
    }
    else{ // argc==1
        OpenMesh::IO::read_mesh(myMesh, "large_irreg_cristobalite_2.obj");
    }

    for(auto v_it=myMesh.vertices_begin(); v_it!=myMesh.vertices_end(); ++v_it)
    {
        Mesh::Point point=myMesh.point(v_it);
        double x = point[0];
        double y = point[1];
        double z = point[2];

        /*point[0] = (y*y + x*x + z*z) * 0.1;
        point[1] = (y*y + x*x + z*z) * 0.1;
        point[2] = (y*y + x*x + z*z) * 0.1;*/
        
        /*point[0] = (y*abs(y) + x*abs(x) + z*abs(z)) * 0.1;
        point[1] = (y*abs(y) + x*abs(x) + z*abs(z)) * 0.1;
        point[2] = (y*abs(y) + x*abs(x) + z*abs(z)) * 0.1;*/

        point[0] = z*abs(z) *0.1 + x;
        point[1] = x*abs(x) *0.1 + y;
        point[2] = y*abs(y) *0.1 + z;

        myMesh.set_point(*v_it, point);
    }

	// initialize window
	int errorCode = initWindow();
	if (errorCode != 0)
		return errorCode;

	// initialize OpenGL pipeline
    // This is also where createObjects() is called
	//initOpenGL();
	initOpenGL(myMesh);

    bool first_iteration=true;
	// For speed computation
	double lastTime = glfwGetTime();
	int nbFrames = 0;
	do {

		// DRAWING POINTS
		renderScene();
        if(first_iteration)
        {
            first_iteration=false;
            double total_time = (clock()-begin)/(double)CLOCKS_PER_SEC;
            std::cout << "Total running time: " << total_time << std::endl;
        }

	} // Check if the ESC key was pressed or the window was closed
	while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
	glfwWindowShouldClose(window) == 0);

	cleanup();


	return 0;
}
