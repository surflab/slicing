
/*

This file is part of slicing. 




slicing is free software: you can redistribute it and/or modify

it under the terms of the GNU General Public License as published by

the Free Software Foundation, either version 3 of the License, or

(at your option) any later version.




slicing is distributed in the hope that it will be useful,

but WITHOUT ANY WARRANTY; without even the implied warranty of

MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the

GNU General Public License for more details.




You should have received a copy of the GNU General Public License

along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <array>
#include <stack>   
#include <sstream>
#include <iostream>
#include <math.h>
// Include GLEW
#include <GL/glew.h>
// Include GLFW
#include <glfw3.h>
// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/vector_angle.hpp>
using namespace glm;
using namespace std;
#include <AntTweakBar.h>

#include <common/shader.hpp>
#include <common/controls.hpp>
#include <common/objloader.hpp>
#include <common/vboindexer.hpp>

#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>

#define PI 3.14159265359

#define HULL 3
#define MAST 4
#define RUDDER 5
#define SAILS 6
#define TOP 7
#define CYLLINDER 8

typedef OpenMesh::PolyMesh_ArrayKernelT<> Mesh;

const int window_width = 1024, window_height = 768;

struct Vertex {
	float Position[4];
	float Color[4];
	float Normal[3];
	void SetPosition(float *coords) {
		Position[0] = coords[0];
		Position[1] = coords[1];
		Position[2] = coords[2];
		Position[3] = 1.0;
	}
	void SetColor(float *color) {
		Color[0] = color[0];
		Color[1] = color[1];
		Color[2] = color[2];
		Color[3] = color[3];
	}
	void SetNormal(float *coords) {
		Normal[0] = coords[0];
		Normal[1] = coords[1];
		Normal[2] = coords[2];
	}
};

struct point {
	float x, y, z;
	point(const float x = 0, const float y = 0, const float z = 0) : x(x), y(y), z(z) {}
	point(float *coords) : x(coords[0]), y(coords[1]), z(coords[2]) {}
	point operator -(const point& a)const {
		return point(x - a.x, y - a.y, z - a.z);
	}
	point operator +(const point& a)const {
		return point(x + a.x, y + a.y, z + a.z);
	}
	point operator *(const float& a)const {
		return point(x*a, y*a, z*a);
	}
	point operator /(const float& a)const {
		return point(x / a, y / a, z / a);
	}
	float* toArray() {
		float* array = new float[4];
        array[0]=x;
        array[1]=y;
        array[2]=z;
        array[3]=1.0f;
		return array;
	}
};

struct edge{
    edge(point a, point b) : p(a), q(b) {}
    point p;
    point q;
    float length(){
        return sqrt((p.x-q.x)*(p.x-q.x) + (p.y-q.y)*(p.y-q.y) + (p.z-q.z)*(p.z-q.z));
    }
};

// function prototypes
int initWindow(void);
void initOpenGL(void);
void initOpenGL(Mesh&);
void loadObject(char*, glm::vec4, Vertex * &, GLushort* &, int);
void create_cyllinder(double radius, double height, Vertex * &, GLushort* &, int);
void create_cyllinder(int num_points_on_boundary, glm::vec4 color, double radius, edge e, Vertex * &, GLushort* &, int);
void create_skeleton(edge e, Vertex* &out_Vertices, GLushort* &out_Indices, int ObjectId);
void gen_z_aligned_cyllinder(int num_points_on_boundary, double radius, double length, glm::vec3 * &, glm::vec3 * &);
void rotate_and_translate(int num_points_on_boundary, edge, glm::vec3 * &, glm::vec3 * &);

void createVAOs(Vertex[], GLushort[], int);
void createObjects(void);
void createObjects(Mesh&);
void updateCamera(void);
void move_octant();
void resetCamera(void);
void renderScene(void);
void cleanup(void);
static void keyCallback(GLFWwindow*, int, int, int, int);
static void mouseCallback(GLFWwindow*, int, int, int);

// GLOBAL VARIABLES
GLFWwindow* window;
bool camera_left = false;
bool camera_right = false;
bool camera_up = false;
bool camera_down = false;
bool useTessEng = true;
bool zoom_in = false;
bool zoom_out = false;
bool slide_up = false;
bool slide_down = false;

bool increase_x = false;
bool decrease_x = false;

bool increase_y = false;
bool decrease_y = false;

bool increase_z = false;
bool decrease_z = false;

bool increase_thickness = false;
bool decrease_thickness = false;

float fov = 45.0f;
int shader = 0;


glm::mat4 gProjectionMatrix;
glm::mat4 gViewMatrix;

std::string gMessage;

GLuint pnQuads;
GLuint pnTriangles;
GLuint standardShading;
GLuint tensorSpline;

int edge_count;
const GLuint NumObjects = 50;	// ATTN: THIS NEEDS TO CHANGE AS YOU ADD NEW OBJECTS

GLuint* VertexArrayId = new GLuint[NumObjects];
GLuint* VertexBufferId = new GLuint[NumObjects];
GLuint* IndexBufferId = new GLuint[NumObjects];

size_t* NumIndices = new size_t[NumObjects];
size_t* VertexBufferSize = new size_t[NumObjects];
size_t* IndexBufferSize = new size_t[NumObjects];

GLuint MatrixID;
GLuint ModelMatrixID;
GLuint ViewMatrixID;
GLuint ProjMatrixID;
GLuint TessProjId;
GLuint TessModelViewID;
GLuint TessLightID;
GLuint StandardModelViewID;
GLuint StandardProjId;
GLuint StandardModelMatrixID;
GLuint StandardViewMatrixID;
GLuint StandardProjMatrixID;
GLuint pnTrianglesModelMatrixID;
GLuint pnTrianglesViewMatrixID;
GLuint pnTrianglesProjMatrixID;
GLuint pnQuadsModelMatrixID;
GLuint pnQuadsViewMatrixID;
GLuint pnQuadsProjMatrixID;
GLuint tensorSplineModelMatrixID;
GLuint tensorSplineViewMatrixID;
GLuint tensorSplineProjMatrixID;
GLuint LightID;
bool use1Dtex = false;
GLuint Light1IDQ;
GLuint Light2IDQ;
GLuint Light1IDT;
GLuint Light2IDT;
GLuint Light1IDS;
GLuint Light2IDS;
GLuint Light1IDTS;
GLuint Light2IDTS;
GLuint pnTriLOD;
GLuint pnQuadLOD;
GLuint evalDensity;
GLuint ClippingPlane;
GLuint Thickness;
GLuint toClip;
GLuint clip_x;
GLuint clip_y;
GLuint clip_z;

Vertex* Verts;
GLushort* Idcs;

GLint gX = 0.0;
GLint gZ = 0.0;
GLint tessNum = 64; // max accepted value is 64
float T[19] = { -3.0f, -2.0f, -1.0f, 0.0f, 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f, 11.0f, 12.0f, 13.0f, 14.0f, 15.0f };

void loadTri(char* file, glm::vec4 color, Vertex* &out_Vertices, GLushort* &out_Indices, int ObjectId)
{
	// Read our .obj file
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	bool res = loadOBJTri(file, vertices, normals);

	std::vector<GLushort> indices;
	std::vector<glm::vec3> indexed_vertices;
	std::vector<glm::vec2> indexed_uvs;
	std::vector<glm::vec3> indexed_normals;
	indexVBO(vertices, normals, indices, indexed_vertices, indexed_normals);

	const size_t vertCount = indexed_vertices.size();
	const size_t idxCount = indices.size();

	// populate output arrays
	out_Vertices = new Vertex[vertCount];
	for (int i = 0; i < vertCount; i++) {
		out_Vertices[i].SetPosition(&indexed_vertices[i].x);
		out_Vertices[i].SetNormal(&indexed_normals[i].x);
		out_Vertices[i].SetColor(&color[0]);
	}
	out_Indices = new GLushort[idxCount];
	for (int i = 0; i < idxCount; i++) {
		out_Indices[i] = indices[i];
	}

	// set global variables!!
	NumIndices[ObjectId] = idxCount;
	VertexBufferSize[ObjectId] = sizeof(out_Vertices[0]) * vertCount;
	IndexBufferSize[ObjectId] = sizeof(GLushort) * idxCount;
}

void loadQuad(char* file, glm::vec4 color, Vertex * &out_Vertices, GLushort* &out_Indices, int ObjectId)
{
	// Read our .obj file
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	bool res = loadOBJQuad(file, vertices, normals);

	std::vector<GLushort> indices;
	std::vector<glm::vec3> indexed_vertices;
	std::vector<glm::vec2> indexed_uvs;
	std::vector<glm::vec3> indexed_normals;
	indexVBO(vertices, normals, indices, indexed_vertices, indexed_normals);

	const size_t vertCount = indexed_vertices.size();
	const size_t idxCount = indices.size();

	// populate output arrays
	out_Vertices = new Vertex[vertCount];
	for (int i = 0; i < vertCount; i++) {
		out_Vertices[i].SetPosition(&indexed_vertices[i].x);
		out_Vertices[i].SetNormal(&indexed_normals[i].x);
		out_Vertices[i].SetColor(&color[0]);
	}
	out_Indices = new GLushort[idxCount];
	for (int i = 0; i < idxCount; i++) {
		out_Indices[i] = indices[i];
	}

	// set global variables!!
	NumIndices[ObjectId] = idxCount;
	VertexBufferSize[ObjectId] = sizeof(out_Vertices[0]) * vertCount;
	IndexBufferSize[ObjectId] = sizeof(GLushort) * idxCount;
}

// creates cyllinder with height and radius at (x,y)=(0,0)
// cyllinder is made of triangles
void create_cyllinder(double radius, double height, Vertex* &out_Vertices, GLushort* &out_Indices, int ObjectId)
{
    const int num_points_on_boundary=10;
    const size_t num_verts=2*(num_points_on_boundary);
    const size_t num_idcs=3*(num_verts-2);
    glm::vec4 color(1.0, 1.0, 1.0, 1.0);

	// populate output arrays
	out_Vertices = new Vertex[num_verts];
	auto out_Vecs = new glm::vec3[num_verts];
	auto out_Normals = new glm::vec3[num_verts];

    float* pos = new float[3];
    float* normal = new float[3];
    float* zero_normal = new float[3];

    normal[2]=0.0;

    zero_normal[0]=0.0;
    zero_normal[1]=0.0;
    zero_normal[2]=0.0;
    
    double theta;

	for (int i = 0; i < num_points_on_boundary; i++) 
    {
        theta = 2.0*PI*(double)i/(double)(num_points_on_boundary-2);

        float n_x=cos(theta);
        float n_y=sin(theta);
        float n_z=0.0;

        float x=radius*cos(theta);
        float y=radius*sin(theta);
        float z=0.0;

        out_Vecs[2*i]=glm::vec3(x, y, z);
        out_Normals[2*i]=glm::vec3(n_x, n_y, n_z);

        z=height;
        out_Vecs[2*i+1]=glm::vec3(x, y, z);
        out_Normals[2*i+1]=glm::vec3(n_x, n_y, n_z);

	}
	for (int i = 0; i < num_points_on_boundary; i++) 
    {
        pos[0]=out_Vecs[2*i].x;
        pos[1]=out_Vecs[2*i].y;
        pos[2]=out_Vecs[2*i].z;

        normal[0]=out_Normals[2*i].x;
        normal[1]=out_Normals[2*i].y;
        normal[2]=out_Normals[2*i].z;

		out_Vertices[2*i].SetPosition(&pos[0]);
		out_Vertices[2*i].SetNormal(&normal[0]);
		out_Vertices[2*i].SetColor(&color[0]);

        pos[0]=out_Vecs[2*i+1].x;
        pos[1]=out_Vecs[2*i+1].y;
        pos[2]=out_Vecs[2*i+1].z;

        normal[0]=out_Normals[2*i+1].x;
        normal[1]=out_Normals[2*i+1].y;
        normal[2]=out_Normals[2*i+1].z;

		out_Vertices[2*i+1].SetPosition(&pos[0]);
		out_Vertices[2*i+1].SetNormal(&normal[0]);
		out_Vertices[2*i+1].SetColor(&color[0]);
	}

    int index=2;
	out_Indices = new GLushort[num_idcs];
    for(int i=3; i<num_idcs; ++i)
    {
        if(i%3==0){
            index-=2;
        }
        out_Indices[i]=index%num_verts;
        index++;
    }

    NumIndices[ObjectId] = num_idcs;
	VertexBufferSize[ObjectId] = sizeof(out_Vertices[0]) * num_verts;
	IndexBufferSize[ObjectId] = sizeof(GLushort) * num_idcs;
}

// creates cyllinder with height and radius at (x,y)=(0,0)
// cyllinder is made of triangles
void create_cyllinder(int num_points_on_boundary, glm::vec4 color, double radius, edge e, Vertex* &out_Vertices, GLushort* &out_Indices, int ObjectId)
{
    //const int num_points_on_boundary=10;
    const size_t num_verts=2*(num_points_on_boundary);
    const size_t num_idcs=3*(num_verts-3);
    //glm::vec4 color(1.0, 1.0, 1.0, 1.0);
    //glm::vec4 color(1.0, 0.0, 0.0, 1.0);

	// populate output arrays
	out_Vertices = new Vertex[num_verts];
	auto out_Vecs = new glm::vec3[num_verts];
	auto out_Normals = new glm::vec3[num_verts];

    float* pos = new float[3];
    float* normal = new float[3];
    float* zero_normal = new float[3];

    normal[2]=0.0;

    zero_normal[0]=0.0;
    zero_normal[1]=0.0;
    zero_normal[2]=0.0;
    
    double theta;
    double length=e.length();

    /*
     *  First generate a cyllinder
     *  along the z-axis of appropriate
     *  radius and length
     */

    gen_z_aligned_cyllinder(num_points_on_boundary, radius, length, out_Vecs, out_Normals);

    /*
     *  Now rotate and translate it
     */

    rotate_and_translate(num_points_on_boundary, e, out_Vecs, out_Normals);

    /*
     *  Now set output and
     *  global variables
     */

	for (int i = 0; i < num_points_on_boundary; i++) 
    {
        pos[0]=out_Vecs[2*i].x;
        pos[1]=out_Vecs[2*i].y;
        pos[2]=out_Vecs[2*i].z;

        normal[0]=out_Normals[2*i].x;
        normal[1]=out_Normals[2*i].y;
        normal[2]=out_Normals[2*i].z;

		out_Vertices[2*i].SetPosition(&pos[0]);
		out_Vertices[2*i].SetNormal(&normal[0]);
		out_Vertices[2*i].SetColor(&color[0]);

        pos[0]=out_Vecs[2*i+1].x;
        pos[1]=out_Vecs[2*i+1].y;
        pos[2]=out_Vecs[2*i+1].z;

        normal[0]=out_Normals[2*i+1].x;
        normal[1]=out_Normals[2*i+1].y;
        normal[2]=out_Normals[2*i+1].z;

		out_Vertices[2*i+1].SetPosition(&pos[0]);
		out_Vertices[2*i+1].SetNormal(&normal[0]);
		out_Vertices[2*i+1].SetColor(&color[0]);
	}

    int index=2;
	out_Indices = new GLushort[num_idcs];

    for(int i=0; i<num_idcs; ++i)
    {
        if(i%3==0){
            index-=2;
        }
        out_Indices[i]=index%num_verts;
        index++;
    }

    NumIndices[ObjectId] = num_idcs;
	VertexBufferSize[ObjectId] = sizeof(out_Vertices[0]) * num_verts;
	IndexBufferSize[ObjectId] = sizeof(GLushort) * num_idcs;
}

void gen_z_aligned_cyllinder(int num_points_on_boundary, double radius, double length, glm::vec3 * &out_Vecs, glm::vec3 * &out_Normals)
{
    double theta=0.0;

    float n_x;
    float n_y;
    float n_z;

    float x;
    float y;
    float z;

	for (int i = 0; i < num_points_on_boundary; i++) 
    {
        theta = 2.0*PI*(double)i/(double)(num_points_on_boundary-2);

        n_x=cos(theta);
        n_y=sin(theta);
        n_z=0.0;

        x=radius*cos(theta);
        y=radius*sin(theta);
        z=0.0;

        out_Vecs[2*i]=glm::vec3(x, y, z);
        out_Normals[2*i]=glm::vec3(n_x, n_y, n_z);

        z=length;

        out_Vecs[2*i+1]=glm::vec3(x, y, z);
        out_Normals[2*i+1]=glm::vec3(n_x, n_y, n_z);
	}
}

/*
 *  Doing rotation and translation at once is faster than doing
 *  each one individually ==> exception to the rule that each
 *  function should only perform one job
 */
void rotate_and_translate(int num_points_on_boundary, edge e, glm::vec3 * &out_Vecs, glm::vec3 * &out_Normals)
{
    point direction{e.p-e.q};
    glm::vec3 shift((e.q).x, (e.q).y, (e.q).z);
    //glm::vec3 shift((e.p).x, (e.p).y, (e.p).z);
    float norm=sqrt(direction.x*direction.x + direction.y*direction.y +direction.z*direction.z);
    direction = direction/norm;  // direction is unit vector from origin parallel with edge e

    glm::vec3 e_vec(direction.x, direction.y, direction.z);
    glm::vec3 z_axis(0.0, 0.0, 1.0);
    float angle = glm::angle(e_vec, z_axis);
    angle = -1.0*angle;
    glm::vec3 e_axis= glm::cross(e_vec, z_axis);

    if(glm::length(e_axis) < 0.00001) // are e_vec and z_axis parallel?
    {
        if(direction.z < 0){
            e_axis = glm::vec3(1.0, 0.0, 0.0);
        }
        else if(direction.z > 0){
            e_axis = glm::vec3(1.0, 0.0, 0.0);
        }
    }

    glm::mat4 rotate_cyl = glm::rotate(angle, e_axis);
    glm::mat4 translate_cyl = glm::translate(shift);

    glm::mat4 transform = translate_cyl*rotate_cyl;

    for (int i = 0; i < num_points_on_boundary; i++) 
    {
        out_Vecs[2*i]=glm::vec3(transform*glm::vec4(out_Vecs[2*i], 1.0));
        out_Normals[2*i]=glm::vec3(rotate_cyl*glm::vec4(out_Normals[2*i], 0.0));

        out_Vecs[2*i+1]=glm::vec3(transform*glm::vec4(out_Vecs[2*i+1], 1.0));
        out_Normals[2*i+1]=glm::vec3(rotate_cyl*glm::vec4(out_Normals[2*i+1], 0.0));
    }
}

void create_skeleton(edge e, Vertex* &out_Vertices, GLushort* &out_Indices, int ObjectId)
{
	out_Vertices = new Vertex[3];
	out_Indices = new GLushort[3];

    glm::vec4 color(1.0, 0.0, 0.0, 1.0);
    glm::vec3 normal(0.0, 0.0, 0.0);

    out_Vertices[0].SetPosition(&((e.p).toArray())[0]);
    out_Vertices[0].SetNormal(&normal[0]);
    out_Vertices[0].SetColor(&color[0]);
    
    out_Vertices[1].SetPosition(&((e.q).toArray())[0]);
    out_Vertices[1].SetNormal(&normal[0]);
    out_Vertices[1].SetColor(&color[0]);
    
    out_Vertices[2].SetPosition(&((e.p).toArray())[0]);
    out_Vertices[2].SetNormal(&normal[0]);
    out_Vertices[2].SetColor(&color[0]);

    out_Indices[0]=0;
    out_Indices[1]=1;
    out_Indices[2]=2;
    
    NumIndices[ObjectId] = 3;
	VertexBufferSize[ObjectId] = sizeof(out_Vertices[0]) * 3.0;
	IndexBufferSize[ObjectId] = sizeof(GLushort) * 3.0;
}

void createObjects(Mesh& myMesh)
{
    double radius=0.1;

    edge_count=myMesh.n_edges();

    std::cout << "There are " << edge_count << " edges in this mesh.\n";
    
    GLuint temp[edge_count] = { 0 };
    VertexArrayId = new GLuint[2*edge_count];
    VertexBufferId = new GLuint[2*edge_count];
    IndexBufferId = new GLuint[2*edge_count];

    NumIndices = new size_t[2*edge_count];
    VertexBufferSize = new size_t[2*edge_count];
    IndexBufferSize= new size_t[2*edge_count];

    glm::vec4 red(1.0, 0.0, 0.0, 1.0);
    glm::vec4 black(0.0, 0.0, 0.0, 1.0);
    glm::vec4 cyan(0.0, 1.0, 1.0, 1.0);

    int index=0;

    for(auto e_it=myMesh.edges_begin(); e_it!=myMesh.edges_end(); ++e_it)
    {
        OpenMesh::HalfedgeHandle he=myMesh.halfedge_handle(*e_it, 0);

        OpenMesh::VertexHandle v_0=myMesh.to_vertex_handle(he);
        OpenMesh::VertexHandle v_1=myMesh.from_vertex_handle(he);

        auto p_0 = myMesh.point(v_0);
        auto p_1 = myMesh.point(v_1);

        edge e{{p_0[0], p_0[1], p_0[2]},{p_1[0], p_1[1], p_1[2]}};

        create_cyllinder(10, cyan, radius, e, Verts, Idcs, index);
        createVAOs(Verts, Idcs, index);
        index++;
    }

    for(auto e_it=myMesh.edges_begin(); e_it!=myMesh.edges_end(); ++e_it)
    {
        OpenMesh::HalfedgeHandle he=myMesh.halfedge_handle(*e_it, 0);

        OpenMesh::VertexHandle v_0=myMesh.to_vertex_handle(he);
        OpenMesh::VertexHandle v_1=myMesh.from_vertex_handle(he);

        auto p_0 = myMesh.point(v_0);
        auto p_1 = myMesh.point(v_1);

        edge e{{p_0[0], p_0[1], p_0[2]},{p_1[0], p_1[1], p_1[2]}};

        //create_skeleton(e, Verts, Idcs, index);
        create_cyllinder(5, black, 0.05*radius, e, Verts, Idcs, index);
        createVAOs(Verts, Idcs, index);
        index++;
    }
}


void createObjects(void)
{
}

float shift=3.5;
float x=0.0;
float y=1.0;
float z=0.0;
float thickness=0.25;

void updateCamera(void)
{
	float delta_angle = 0.005;
	glm::vec3 vertical = glm::vec3(0.0, 1.0, 0.0);
	glm::vec3 horizontal = glm::vec3(gViewMatrix[0][0], gViewMatrix[1][0], gViewMatrix[2][0]);

	if (camera_left) {
		gViewMatrix = glm::rotate(gViewMatrix, delta_angle, vertical);
	}
    if (camera_right) {
		gViewMatrix = glm::rotate(gViewMatrix, -delta_angle, vertical);
	}
    if (camera_down) {
		gViewMatrix = glm::rotate(gViewMatrix, -delta_angle, horizontal);
	}
    if (camera_up) {
		gViewMatrix = glm::rotate(gViewMatrix, delta_angle, horizontal);
	}
    if(slide_up){
        shift -= 0.01;
    }
    if(slide_down){
        shift += 0.01;
    }
    if(increase_x){
        x += 0.01;
    }
    if(decrease_x){
        x -= 0.01;
    }
    if(increase_y){
        y += 0.01;
    }
    if(decrease_y){
        y -= 0.01;
    }
    if(increase_z){
        z += 0.01;
    }
    if(decrease_z){
        z -= 0.01;
    }
    if(increase_thickness){
        thickness += 0.01;
    }
    if(decrease_thickness){
        thickness -= 0.01;
    }
    if (zoom_in) {
		if (fov > 44.0)
		{
			fov = fov - 0.01;
			gProjectionMatrix = glm::perspective(fov, 4.0f / 3.0f, 0.1f, 100.0f);
		}
	}
    if (zoom_out) {
		fov = fov + 0.01;
		gProjectionMatrix = glm::perspective(fov, 4.0f / 3.0f, 0.1f, 100.0f);
	}
}

int octant=0;
int show_x=1;
int show_y=1;
int show_z=1;
void move_octant(){
    int oct_count=4;
    if(octant%oct_count==0)
    {
        show_x=1;
        show_y=1;
        show_z=1;
    }
    else if(octant%oct_count==1)
    {
        show_x=0;
        show_y=1;
        show_z=1;
    }
    else if(octant%oct_count==2)
    {
        show_x=0;
        show_y=1;
        show_z=0;
    }
    else if(octant%oct_count==3)
    {
        show_x=1;
        show_y=1;
        show_z=0;
    }
    octant++;
}

void resetCamera(void)
{
	// Camera matrix
	gViewMatrix = glm::lookAt(glm::vec3(10.0, 10.0, 10.0f),	// eye
		glm::vec3(0.0, 0.0, 0.0),	// center
		glm::vec3(0.0, 1.0, 0.0));	// up
	createObjects();
}


void renderScene(void)
{
	//ATTN: DRAW YOUR SCENE HERE. MODIFY/ADAPT WHERE NECESSARY!

	updateCamera();

	// Background
	//glClearColor(0.0f, 0.0f, 0.2f, 0.0f);
	//glClearColor(0.6f, 0.7f, 1.0f, 0.0f);
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);

	// Re-clear the screen for real rendering
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(pnTriangles); {
        glm::vec3 light1Pos = glm::vec3(-6.0f, 6.0f, -6.0f); // red
        glm::vec3 light2Pos = glm::vec3(6.0f, -6.0f, 6.0f);  // blue
        glUniform3f(Light1IDT, light1Pos.x, light1Pos.y, light1Pos.z);
        glUniform3f(Light2IDT, light2Pos.x, light2Pos.y, light2Pos.z);
        glm::mat4 ModelMatrix = glm::mat4(1.0);
        glUniformMatrix4fv(pnTrianglesModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);
        glUniformMatrix4fv(pnTrianglesViewMatrixID, 1, GL_FALSE, &gViewMatrix[0][0]);
        glUniformMatrix4fv(pnTrianglesProjMatrixID, 1, GL_FALSE, &gProjectionMatrix[0][0]);
        glUniform1i(pnTriLOD, 64);
        
        glm::vec4 plane_L = glm::vec4(x, y, z, shift); 
        glUniform4f(ClippingPlane, plane_L.x, plane_L.y, plane_L.z, plane_L.w);
        glUniform1f(Thickness, thickness);

        // Order of the draw loops is important!

        // Draw slice
        glUniform1i(toClip, 1);
        glUniform1i(clip_x, show_x);
        glUniform1i(clip_y, show_y);
        glUniform1i(clip_z, show_z);
        for(int i=0; i<edge_count; ++i)
        {
            glBindVertexArray(VertexArrayId[i]); // draw with pn triangles
            glPatchParameteri(GL_PATCH_VERTICES, 3);
            glPolygonMode( GL_FRONT_AND_BACK, GL_FILL);  // patches
            //glPolygonMode( GL_FRONT_AND_BACK, GL_LINE);  // wireframe
            glDrawElements(GL_PATCHES, NumIndices[i], GL_UNSIGNED_SHORT, (void*)0);
        }

        // Draw transparent cube
        /*glUniform1i(toClip, 0);
        for(int i=0; i<edge_count; ++i)
        {
            glBindVertexArray(VertexArrayId[i]); // draw with pn triangles
            glPatchParameteri(GL_PATCH_VERTICES, 3);
            //glPolygonMode( GL_FRONT_AND_BACK, GL_LINE);  // wireframe
            glDrawElements(GL_PATCHES, NumIndices[i], GL_UNSIGNED_SHORT, (void*)0);
        }*/

        // Draw graph of cube
        glUniform1i(toClip, 0);
        /*glUniform1i(clip_x, 0);
        glUniform1i(clip_y, 0);
        glUniform1i(clip_z, 0);*/
        for(int i=edge_count; i<2*edge_count; ++i)
        {
            glBindVertexArray(VertexArrayId[i]); // draw with pn triangles
            glPatchParameteri(GL_PATCH_VERTICES, 3);
            glPolygonMode( GL_FRONT_AND_BACK, GL_FILL);  // patches
            //glPolygonMode( GL_FRONT_AND_BACK, GL_LINE);  // wireframe
            glDrawElements(GL_PATCHES, NumIndices[i], GL_UNSIGNED_SHORT, (void*)0);
        }
    }

	glUseProgram(0);

	// Swap buffers
	glfwSwapBuffers(window);
	glfwPollEvents();
}


int initWindow(void)
{
	// Initialise GLFW
	if (!glfwInit()) {
		fprintf(stderr, "Failed to initialize GLFW\n");
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow(window_width, window_height, "Youngquist, Jeremy (5190-9347)", NULL, NULL);
	if (window == NULL) {
		fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n");
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

	// Set up inputs
	glfwSetKeyCallback(window, keyCallback);
	glfwSetMouseButtonCallback(window, mouseCallback);

	return 0;
}

void initOpenGL(Mesh& myMesh)
{

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);
	// Cull triangles which normal is not towards the camera
	//glEnable(GL_CULL_FACE);
	// Enable program point size
	glEnable(GL_PROGRAM_POINT_SIZE);

    //glEnable(GL_CLIP_DISTANCE0);

    // Enable blending
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Projection matrix : 45� Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	gProjectionMatrix = glm::perspective(45.0f, 4.0f / 3.0f, 0.1f, 100.0f);

	// Camera matrix
	gViewMatrix = glm::lookAt(glm::vec3(10, 0, 10),	// eye
		glm::vec3(0.0, 0.0, 0.0),	// center
		glm::vec3(0.0, 1.0, 0.0));	// up

	/*gViewMatrix = glm::lookAt(glm::vec3(10,10,10),	// eye
		glm::vec3(0.0,2, 0.0),	// center
		glm::vec3(0.0, 1.0, 0.0));	// up*/


	// Create and compile our GLSL program from the shaders
	pnTriangles = LoadTessShaders("pn_tri_vs.vertexshader", "pn_tri_frag.fragmentshader", "pn_tri_ev.tesshader", "pn_tri_cntrl.tesshader");

	pnTrianglesModelMatrixID = glGetUniformLocation(pnTriangles, "M");
	pnTrianglesViewMatrixID = glGetUniformLocation(pnTriangles, "V");
	pnTrianglesProjMatrixID = glGetUniformLocation(pnTriangles, "P");
	Light1IDT = glGetUniformLocation(pnTriangles, "LightPosition_worldspace");
	Light2IDT = glGetUniformLocation(pnTriangles, "Light2Position_worldspace");
	pnTriLOD = glGetUniformLocation(pnTriangles, "lod");

	Thickness = glGetUniformLocation(pnTriangles, "thickness");
	toClip = glGetUniformLocation(pnTriangles, "toClip");
	clip_x = glGetUniformLocation(pnTriangles, "clip_x");
	clip_y = glGetUniformLocation(pnTriangles, "clip_y");
	clip_z = glGetUniformLocation(pnTriangles, "clip_z");
	ClippingPlane = glGetUniformLocation(pnTriangles, "ClippingPlane");

	createObjects(myMesh);
}

void createVAOs(Vertex Vertices[], unsigned short Indices[], int ObjectId) {

	GLenum ErrorCheckValue = glGetError();
	const size_t VertexSize = sizeof(Vertices[0]);
	const size_t RgbOffset = sizeof(Vertices[0].Position);
	const size_t Normaloffset = sizeof(Vertices[0].Color) + RgbOffset;

	// Create Vertex Array Object
	glGenVertexArrays(1, &VertexArrayId[ObjectId]);	//
	glBindVertexArray(VertexArrayId[ObjectId]);		//

	// Create Buffer for vertex data
	glGenBuffers(1, &VertexBufferId[ObjectId]);
	glBindBuffer(GL_ARRAY_BUFFER, VertexBufferId[ObjectId]);
	glBufferData(GL_ARRAY_BUFFER, VertexBufferSize[ObjectId], Vertices, GL_STATIC_DRAW);

	// Create Buffer for indices
	if (Indices != NULL) {
		glGenBuffers(1, &IndexBufferId[ObjectId]);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IndexBufferId[ObjectId]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, IndexBufferSize[ObjectId], Indices, GL_STATIC_DRAW);
	}

	// Assign vertex attributes
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, VertexSize, 0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, VertexSize, (GLvoid*)RgbOffset); 
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, VertexSize, (GLvoid*)Normaloffset);

	glEnableVertexAttribArray(0);	// position
	glEnableVertexAttribArray(1);	// color
	glEnableVertexAttribArray(2);	// normal

	// Disable our Vertex Buffer Object 
	glBindVertexArray(0);

	ErrorCheckValue = glGetError();
	if (ErrorCheckValue != GL_NO_ERROR)
	{
		fprintf(
			stderr,
			"ERROR: Could not create a VBO: %s \n",
			gluErrorString(ErrorCheckValue)
			);
	}
}

void cleanup(void)
{
	// Cleanup VBO and shader
	for (int i = 0; i < NumObjects; i++) {
		glDeleteBuffers(1, &VertexBufferId[i]);
		glDeleteBuffers(1, &IndexBufferId[i]);
		glDeleteVertexArrays(1, &VertexArrayId[i]);
	}
	glDeleteProgram(standardShading);
	glDeleteProgram(pnTriangles);
	glDeleteProgram(pnQuads);

	// Close OpenGL window and terminate GLFW
	glfwTerminate();
}

static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	// ATTN: MODIFY AS APPROPRIATE
	if (action == GLFW_PRESS || action == GLFW_RELEASE) {
		switch (key)
		{
		case GLFW_KEY_R:
			resetCamera();
			break;
		case GLFW_KEY_UP:
			if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT))
			{
				zoom_in = !zoom_in;
			}
            else if(glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT))
			{
                slide_up=!slide_up;
			}
            else if(glfwGetKey(window, GLFW_KEY_X))
			{
                increase_x = !increase_x;
			}
            else if(glfwGetKey(window, GLFW_KEY_Y))
			{
                increase_y = !increase_y;
			}
            else if(glfwGetKey(window, GLFW_KEY_Z))
			{
                increase_z = !increase_z;
			}
            else if(glfwGetKey(window, GLFW_KEY_T))
			{
                increase_thickness = !increase_thickness;
			}
			else
			{
				camera_up = !camera_up;
			}
			break;
		case GLFW_KEY_DOWN:
			if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT))
			{
				zoom_out = !zoom_out;
			}
            else if(glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT))
			{

                slide_down=!slide_down;
			}
            else if(glfwGetKey(window, GLFW_KEY_X))
			{
                decrease_x = !decrease_x;
			}
            else if(glfwGetKey(window, GLFW_KEY_Y))
			{
                decrease_y = !decrease_y;
			}
            else if(glfwGetKey(window, GLFW_KEY_Z))
			{
                decrease_z = !decrease_z;
			}
            else if(glfwGetKey(window, GLFW_KEY_T))
			{
                decrease_thickness = !decrease_thickness;
			}
			else
			{
				camera_down = !camera_down;
			}
			break;
		case GLFW_KEY_O:
            if(action == GLFW_PRESS){
                move_octant();
            }
			break;
		case GLFW_KEY_LEFT:
			camera_left = !camera_left;
			break;
		case GLFW_KEY_RIGHT:
			camera_right = !camera_right;
			break;
		case GLFW_KEY_SPACE: {
			if (action != GLFW_RELEASE)
			{
				shader++;
			}
		}
		default:
			break;
		}
	}
}

static void mouseCallback(GLFWwindow* window, int button, int action, int mods)
{
}

