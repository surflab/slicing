
This code has been tested on Ubuntu 16.04

Input: An .off file representing the discrete representation of (3d) microstrucure.
Output: An opengl drawing of the mircrostructure with a cylinder at each edge.  This reperesentation allows us to see and modify slices through the mictrostrucure.

Dependencies:

OpenMesh: https://www.openmesh.org/download/
Follow the instructions to build and install it on your system : https://www.openmesh.org/media/Documentations/OpenMesh-Doc-Latest/a03933.html


To build:

cd Build
cmake ../Source
make

To run:
./slicing $FILENAME
./slicing with no filename will display default file "large_irreg_cristobalite_2.obj"

There are example test files in the "test_objs" folder.
We suggest using the default file "large_irreg_cristobalie_2.obj" which will be shown if you do not provide a filename.

User Interface:
Arrow keys rotate camera around origin.
Left Shift + arrow up or down zooms.
Right Shift + arrow up or down moves slice plane along plane normal.
x, y, z key + arrow up or down changes the coefficient of x, y, or z component of plane normal.
o changes which octant is being shown.
t + arrow up or down increases or decreases the thickness of the slice.

License:

slicing is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. The GNU GPL license can be found at http://www.gnu.org/licenses/ or in the included file GNU_GPL_License.txt.
